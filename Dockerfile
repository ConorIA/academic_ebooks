FROM alpine:edge

MAINTAINER Conor I. Anderson <conor@conr.ca>

RUN apk add --no-cache python3

COPY requirements.txt /tmp/requirements.txt

RUN ln -s /usr/bin/python3 /usr/bin/python &&\
  pip3 install -r /tmp/requirements.txt
