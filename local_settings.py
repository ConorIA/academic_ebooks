'''
Local Settings for a heroku_ebooks account.
'''

SOURCE_ACCOUNTS = ["AGWobserver"]
ODDS = 4
ORDER = 2
SOURCE_EXCLUDE = r'^$'
DEBUG = False
STATIC_TEST = False 
TEST_SOURCE = ".txt"
SCRAPE_URL = True 
SRC_URL = ['https://web.archive.org/web/20171229060138/http://www.pnas.org:80/reports/most-cited', 'https://www.altmetric.com/top100/2016/']
WEB_CONTEXT = ['span', 'h2']
WEB_ATTRIBUTES = [{'class': 'cit-title'}, {}]
TWEET_ACCOUNT = "academic_ebooks"
