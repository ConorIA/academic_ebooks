## [@academic_ebooks](https://twitter.com/academic_ebooks)

This is an implementation of [Heroku ebooks](https://github.com/tommeagher/heroku_ebooks) that creates random science article titles from a few sources: 

- [@agwobserver](https://twitter.com/agwobserver)
- The Altmetric [top 100 from 2016](https://www.altmetric.com/top100/2016/)
- PNAS Top 100 [(Archive)](https://web.archive.org/web/20171229060138/http://www.pnas.org:80/reports/most-cited)
